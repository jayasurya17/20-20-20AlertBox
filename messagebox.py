import pymsgbox
import time

start_time = time.time()
while True:
    cur_time = time.time()
    time_elapsed = int(cur_time - start_time)
    # print time_elapsed
    if time_elapsed == 1220:
        pymsgbox.alert('Take a 20 seconds break', '20-20-20 rule')
        start_time = time.time()
    elif time_elapsed > 1220:
        start_time = time.time()
